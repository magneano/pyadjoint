.. _fenics-adjoint-manual:

*************************
How to use fenics-adjoint
*************************

.. sectionauthor:: Patrick E. Farrell <patrick.farrell@maths.ox.ac.uk>,
                   Simon W. Funke <simon@simula.no>

.. toctree::

   tutorial
   verification
   debugging
   parallel
   functionals
   custom_functions
